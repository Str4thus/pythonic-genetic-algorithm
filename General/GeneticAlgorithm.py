from General.Chromosome import Encoding, Chromosome
from Evolution.Crossover import Crossover
from Evolution.Selection import Selection
from random import choice, random


class GeneticAlgorithm:
    iterations = 200

    population_size = 600
    chromosome_size = 7

    def __init__(self, fitness, encoding: Encoding, crossover: Crossover, selection: Selection):
        self._solution = None
        self.current_population = []

        for i in range(GeneticAlgorithm.population_size):
            self.current_population.append(Chromosome(encoding, GeneticAlgorithm.chromosome_size))

        for chromosome in self.current_population:
            chromosome.fitness = fitness(chromosome)

        self.current_population.sort(key=lambda x: x.fitness, reverse=True)  # fittest chromosomes at the beginning of the list


        for i in range(GeneticAlgorithm.iterations):
            new_population = []

            new_population += selection.select(self.current_population)

            while (len(new_population) < len(self.current_population)):
                new_population.append(crossover.crossover(choice(self.current_population), choice(self.current_population)))

            for chromosome in new_population:
                chromosome.mutate()

            for chromosome in new_population:
                chromosome.fitness = fitness(chromosome)


            new_population.sort(key=lambda x: x.fitness, reverse=True) # fittest chromosomes at the beginning of the list
            self._solution = new_population[0]
            self.current_population = new_population

    def solution(self):
        return self._solution