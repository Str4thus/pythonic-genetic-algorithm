from random import randint, shuffle, uniform, random
from enum import Enum

class Encoding(Enum):
    Binary = 1,
    Permutation = 2,
    Value = 3

class Chromosome:
    def __init__(self, encoding: Encoding, size: int, value_range: range = None):
        self._encoding = encoding
        self.value_range = value_range
        self.genes = []
        self.fitness = None

        if (encoding is Encoding.Binary):
            for i in range(size):
                self.genes.append(randint(0, 1))

        elif (encoding is Encoding.Permutation):
            for i in range(size):
                self.genes.append(i)
            shuffle(self.genes)

        elif (encoding is Encoding.Value):
            for i in range(size):
                self.genes.append(uniform(value_range.start, value_range.stop))

    def mutate(self):
        if (self.encoding is Encoding.Binary):
            for i in range(self.size):
                if (random() > .2):
                    self.genes[i] = 0 if self.genes[i] is 1 else 1

        elif (self.encoding is Encoding.Permutation):
            for i in range(self.size):
                if (random() > .2):
                    other = randint(0, self.size-1)

                    self.genes[i], self.genes[other] = self.genes[other], self.genes[i]

        elif (self.encoding is Encoding.Value):
            for i in range(self.size):
                if (random() > .2):
                    self.genes[i] += uniform(-1, 1)

    @property
    def encoding(self):
        return self._encoding

    @property
    def size(self):
        return len(self.genes)