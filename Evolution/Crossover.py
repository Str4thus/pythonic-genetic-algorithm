from General.Chromosome import Chromosome, Encoding
from random import random
from enum import Enum

class CrossoverType(Enum):
    SPX = 1,
    TPX = 2,
    UX = 3

class Crossover:
    def __init__(self, type: CrossoverType):
        self._type = type

    def _single_point_crossover(self, a: Chromosome, b: Chromosome) -> Chromosome:
        if (a.encoding is not Encoding.Permutation):
            child = Chromosome(a.encoding, a.size)
            crossover_point = round(random() * a.size)

            for i in range(a.size):
                if (i < crossover_point):
                    child.genes[i] = a.genes[i]
                else:
                    child.genes[i] = b.genes[i]

            return child

        else:
            child = Chromosome(a.encoding, a.size)
            crossover_point = round(random() * a.size)

            for i in range(a.size):
                if (i < crossover_point):
                    child.genes[i] = a.genes[i]
                else:
                    for j in range(b.size):
                        if (not child.genes.__contains__(b.genes[j])):
                            child.genes[i] = b.genes[j]

            return child


    def _two_point_crossover(self, a: Chromosome, b: Chromosome) -> Chromosome:
        child = Chromosome(a.encoding, a.size)
        first_crossover_point = round(random() * a.size)
        second_crossover_point = round(random() * a.size)

        while (first_crossover_point == second_crossover_point):
            second_crossover_point = round(random() * a.size)

        for i in range(a.size):
            if (i < min(first_crossover_point, second_crossover_point) or
                    i > max(first_crossover_point, second_crossover_point)):
                child.genes[i] = a.genes[i]
            else:
                child.genes[i] = b.genes[i]

        return child

    def _uniform_crossover(self, a: Chromosome, b: Chromosome) -> Chromosome:
        child = Chromosome(a.encoding, a.size)

        for i in range(a.size):
            if (random() > .5):
                child.genes[i] = a.genes[i]
            else:
                child.genes[i] = b.genes[i]

        return child

    def crossover(self, a: Chromosome, b: Chromosome) -> Chromosome:
        if (self._type is CrossoverType.SPX):
            return self._single_point_crossover(a, b)

        elif (self._type is CrossoverType.TPX):
            return self._two_point_crossover(a, b)

        elif (self._type is CrossoverType.UX):
            return self._uniform_crossover(a, b)

    @property
    def type(self):
        return self._type