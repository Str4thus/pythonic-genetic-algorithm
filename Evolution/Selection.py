from enum import Enum


class SelectionType(Enum):
    Roulette = 1,
    Rank = 2,
    Elitism = 3

class Selection:
    def __init__(self, type: SelectionType):
        self.type = type

    def elitism_selection(self, population: []) -> []:
        selected_chromosomes = []

        last_index = round(len(population) * .3)

        for i in range(last_index):
            selected_chromosomes.append(population[i])

        return selected_chromosomes

    def select(self, population: []) -> []:
        if (self.type is SelectionType.Elitism):
            return self.elitism_selection(population)
