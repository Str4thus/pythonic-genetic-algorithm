from General.GeneticAlgorithm import *
from General.Chromosome import *
from Evolution.Selection import *
from Evolution.Crossover import *

from math import sqrt

def knapsack_fitness(chromosome: Chromosome, log = False):
    weight = 0
    value = 0

    for i in range(chromosome.size):
        if (i == 0 and chromosome.genes[i] == 1):
            weight += 10
            value += 11

        elif (i == 1 and chromosome.genes[i] == 1):
            weight += 4
            value += 8

        elif (i == 2 and chromosome.genes[i] == 1):
            weight += 5
            value += 6

        elif (i == 3 and chromosome.genes[i] == 1):
            weight += 2
            value += 12

        elif (i == 4 and chromosome.genes[i] == 1):
            weight += 8
            value += 5

        elif (i == 5 and chromosome.genes[i] == 1):
            weight += 2
            value += 8

        elif (i == 6 and chromosome.genes[i] == 1):
            weight += 6
            value += 4

        elif (i == 7 and chromosome.genes[i] == 1):
            weight += 7
            value += 13

    if weight > 15:
        return 0

    if value == 0:
        return 0

    if (log):
        print("Weight: " + str(weight))
        print("Value: " + str(value))

    return 1 - 1 / value



def tsp_fitness(chromosome: Chromosome, log = False):
    cities = [(0, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1), (1, -1)]
    total_distance = 0

    for i in range(1, chromosome.size):
        total_distance += sqrt((cities[chromosome.genes[i-1]][0] - cities[chromosome.genes[i]][0]) ** 2 +
                               (cities[chromosome.genes[i-1]][1] - cities[chromosome.genes[i]][1]) ** 2)

    total_distance += sqrt((cities[chromosome.genes[-1]][0] - cities[chromosome.genes[0]][0])**2 +
                           (cities[chromosome.genes[-1]][1] - cities[chromosome.genes[0]][1])**2)

    return 1 / total_distance


crossover = Crossover(CrossoverType.SPX)
selection = Selection(SelectionType.Elitism)
"""
genetic_algorithm = GeneticAlgorithm(knapsack_fitness, Encoding.Permutation, crossover, selection)
solution = genetic_algorithm.solution()

print(solution.genes)
knapsack_fitness(solution, log=True)
"""

genetic_algorithm = GeneticAlgorithm(tsp_fitness, Encoding.Permutation, crossover, selection)
solution = genetic_algorithm.solution()

print (solution.genes)